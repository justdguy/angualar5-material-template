import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';

export const adminCommps = [AdminDashboardComponent]

const routes: Routes = [{
  path: '',
  component: AdminComponent,
  children: [{ 
    path: 'dashboard', 
    loadChildren: './admin-dashboard/admin-dashboard.module#AdminDashboardModule'
    // component: AdminDashboardComponent, 
  },{ 
    path: '', 
    redirectTo: 'dashboard', 
    pathMatch: 'full' 
  }]
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }RouterModule