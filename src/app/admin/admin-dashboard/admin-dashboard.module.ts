import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AdminDashboardComponent } from './admin-dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
          path: '', component: AdminDashboardComponent
      }
  ]),
  ],
  declarations: [AdminDashboardComponent]
})
export class AdminDashboardModule { }
