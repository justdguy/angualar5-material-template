import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule} from '../demo-material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { AdminSidebarComponent } from './admin-sidebar/admin-sidebar.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { SpinnerComponent } from './../shared/spinner.component';
import  { AdminMenuItems } from './menu-items/admin-menu-items'

@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  declarations: [AdminComponent, AdminSidebarComponent, AdminHeaderComponent],
  providers: [ AdminMenuItems ]
})
export class AdminModule { }
