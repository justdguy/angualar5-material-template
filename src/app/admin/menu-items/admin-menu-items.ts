import { Injectable } from '@angular/core';

export interface AdminMenu {
  state: string;
  name: string;
  type: string;
  icon: string;
}

const MENUITEMS = [
    {state: 'starter', name: 'Starter Page', type: 'link', icon: 'av_timer' },
    {state: 'admin', type: 'link', name: 'Admin', icon: 'crop_7_5'},
    
]; 

@Injectable()

export class AdminMenuItems {
  getMenuitem(): AdminMenu[] {
    return MENUITEMS;
  }

}
