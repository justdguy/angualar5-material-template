import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { FullComponent } from './layouts/full/full.component';

export const AppRoutes: Routes = [{
  path: '',
  component: AppComponent,
  children: [{ 
    path: '', 
    redirectTo: '/starter', 
    pathMatch: 'full' 
  }, {
    path: '',
    loadChildren: './material-component/material.module#MaterialComponentsModule'
  }, {
    path: 'starter',
    loadChildren: './starter/starter.module#StarterModule'
  },{
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule'
  }]
}];

